export interface IConfig {
  PORT: string;
  NODE_ENV: string;
  CORS_ORIGIN: string;
}
