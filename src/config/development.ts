import dotenv from "dotenv";
import { IConfig } from "../types/config.types";
const { NODE_ENV } = process.env;

if (NODE_ENV !== "production") dotenv.config();

const devConfig: IConfig = {
  PORT: `${process.env.PORT}`,
  NODE_ENV: `${process.env.NODE_ENV}`,
  CORS_ORIGIN: `${process.env.CORS_ORIGIN}`,
};

export default devConfig;
