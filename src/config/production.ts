import { IConfig } from "../types/config.types";

const prodConfig: IConfig = {
  PORT: `${process.env.PORT}`,
  NODE_ENV: `${process.env.NODE_ENV}`,
  CORS_ORIGIN: `${process.env.CORS_ORIGIN}`,
};

export default prodConfig;
