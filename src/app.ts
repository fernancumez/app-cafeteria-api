import express, { Request, Response } from "express";
import compression from "compression";
import cors, { CorsOptions } from "cors";
import config from "./config";
import morgan from "morgan";

const app = express();

app.set("port", parseInt(config.PORT));

app.use(cors());
app.use(compression());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/api", (req: Request, res: Response) => {
  try {
    res.status(200).json({ message: "Hello world!" });
  } catch (error) {
    res.status(400).json({ error });
  }
});

export default app;
